<?php

require_once(dirname(dirname(dirname(__FILE__))).'/config.php');
require_once(dirname(__FILE__).'/lib.php');

$id = optional_param('id', 0, PARAM_INT); // Course_module ID, or
$n  = optional_param('n', 0, PARAM_INT);  // ... pluginmaster instance ID - it should be named as the first character of the module.

if ($id) {
    $cm         = get_coursemodule_from_id('pluginmaster', $id, 0, false, MUST_EXIST);
    $course     = $DB->get_record('course', array('id' => $cm->course), '*', MUST_EXIST);
    $pluginmaster  = $DB->get_record('pluginmaster', array('id' => $cm->instance), '*', MUST_EXIST);
} else if ($n) {
    $pluginmaster  = $DB->get_record('pluginmaster', array('id' => $n), '*', MUST_EXIST);
    $course     = $DB->get_record('course', array('id' => $pluginmaster->course), '*', MUST_EXIST);
    $cm         = get_coursemodule_from_instance('pluginmaster', $pluginmaster->id, $course->id, false, MUST_EXIST);
} else {
    error('You must specify a course_module ID or an instance ID');
}

require_login($course, true, $cm);

$event = \mod_pluginmaster\event\course_module_viewed::create(array(
    'objectid' => $PAGE->cm->instance,
    'context' => $PAGE->context,
));
$event->add_record_snapshot('course', $PAGE->course);
$event->add_record_snapshot($PAGE->cm->modname, $pluginmaster);
$event->trigger();

// Print the page header.

$PAGE->set_url('/mod/pluginmaster/add.php', array('id' => $cm->id));
$PAGE->set_title(format_string($pluginmaster->name));
$PAGE->set_heading(format_string($course->fullname));
//$PAGE->set_heading("Création de conférence");

/*
 * Other things you may want to set - remove if not needed.
 * $PAGE->set_cacheable(false);
 * $PAGE->set_focuscontrol('some-html-id');
 * $PAGE->add_body_class('pluginmaster-'.$somevar);
 */

// Output starts here.
echo $OUTPUT->header();

// Conditions to show the intro can change to look for own settings or whatever.
if ($pluginmaster->intro) {
    echo $OUTPUT->box(format_module_intro('pluginmaster', $pluginmaster, $cm->id), 'generalbox mod_introbox', 'pluginmasterintro');
}

// Replace the following lines with you own code.
//echo $OUTPUT->heading('Yay! It works!');


//inclusion du fichier simplehtml_form.php
include('form/add_form.php');
//creation d‘un objet simplehtml_form 
$mform = new add_form("add.php?id=".$_GET['id']);
//Le traitement et l'affichage des formulaires sont effectués ici
if($mform->is_cancelled()) {
    //Effectuez l'opération d'annulation de formulaire, si l‘utilisateur à cliqué sur le 	//bouton d'annulation du formulaire
} else if ($fromform = $mform->get_data()) {

	 $p=new stdClass();
	 // $p->titre=$_POST['titre'];
	 // $p->description=$_POST['description'];
	 $p->user=$USER->id;
	 $p->name=$fromform->nom;
	 $p->welcome=$fromform->welcome;
	 $p->meetingid=uniqid();
	 $p->maxparticipants=$fromform->participants;
	 $p->attendeepw=$fromform->attendeepw;
	 $p->moderatorpw=$fromform->moderatorpw;
	 $p->voicebridge="";
	 $p->webvoice="";
	 $p->dialnumber="";
	 
	 $p->datedebut=$fromform->dateDebut;

	 //$instance=$DB->get_record('pluginmaster', array('id' => $cm->instance), '*', MUST_EXIST);
	 $p->instance=$cm->instance;
	 
	 //echo $p->page_courant.$p->titre;
	 $idRec=$DB->insert_record('conference',$p, true);
         $conference=$DB->get_record('conference', array('meetingid' => $p->meetingid), '*', MUST_EXIST);
	 inviteAllUser($conference);
         redirection("view.php?id=".$_GET['id']."&message=Votre présentation a été ajouter avec succés");


} else {
  // Cette branche est exécutée si le formulaire est soumis, mais les données ne sont pas validées et le formulaire doit être reproduit automatiquement   
// ou sur le premier affichage du formulaire.
 //Définir les données par défaut (le cas échéant)
  //$mform->set_data($toform);
  //afficher le formulaire
  $mform->display();
}

function redirection($filename) {
    if (!headers_sent())
        header('Location: '.$filename);
    else {
        echo '<script type="text/javascript">';
        echo 'window.location.href="'.$filename.'";';
        echo '</script>';
        echo '<noscript>';
        echo '<meta http-equiv="refresh" content="0;url='.$filename.'" />';
        echo '</noscript>';
    }
}

function inviteAllUser($conference){
	global $course;
        $users = get_enrolled_users(context_course::instance($course->id), '');

		if($users!=null){
			foreach( $users as $us){ 
				send_message($us,$conference);
			}
		}
}

function send_message($user,$conference){
global $USER,$id;
$message = new \core\message\message();
$message->component = 'moodle';
$message->name = 'instantmessage';
$message->userfrom = $USER;
$message->userto = $user;
$message->courseid = $id;
$message->subject = 'Invitation a la conférence '.$conference->name;

$message->fullmessageformat = FORMAT_MARKDOWN;

$mes='<p>Vous etes invite a la conference '.$conference->name.'<br> 
Pour joindre la conference veuillez cliquer sur le lien suivant
<a href="../mod/pluginmaster/join_invite.php?id='.$id.'&conference='.$conference->id.'">Joindre</a></p>';
$message->fullmessagehtml = $mes;
$message->fullmessage = $mes;
//$message->smallmessage = 'small message';
$message->notification = '0';
$message->contexturl = '';
$message->contexturlname = 'Conference';
$message->replyto = "latyrndiaye@esp.sn";
//$content = array('*' => array('header' => ' Conference ', 'footer' => ' test ')); // Extra content for specific processor
//$message->set_additional_content('email', $content);
$messageid = message_send($message);
echo("idmessage send=".$messageid);
}

// Finish the page.
echo $OUTPUT->footer();
?>
