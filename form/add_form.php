<?php
require_once("$CFG->libdir/formslib.php");
class add_form extends moodleform {
	public function definition(){
       	global $CFG;
		$mform = $this->_form;
		$mform->addElement('header', 'general', get_string("newconference","pluginmaster"));
		$mform->addElement('text', 'nom',get_string("conferencename","pluginmaster"), 'maxlength="100" size="20" ');
		$mform->setType('nom', PARAM_TEXT);
		$options = array();
		for($i=2;$i<30;$i++)
			$options[$i.""]=$i." participants";
		$select = $mform->addElement('select', 'participants', get_string('participants', 'pluginmaster'), $options);
				
		$mform->addElement('textarea', 'welcome', get_string('conferencewelcome', 'pluginmaster'));
		$mform->setType('welcome', PARAM_TEXT);
		$mform->addElement('date_time_selector', 'dateDebut', get_string('begindate', 'pluginmaster'));
		
		$mform->addElement('passwordunmask', 'attendeepw', get_string('attenderpw', 'pluginmaster'));
		$mform->addElement('passwordunmask', 'moderatorpw', get_string('moderatorpw', 'pluginmaster'));
		
		$this->add_action_buttons(true);
		
		
	}
	function validation($data, $files) {
        	    return array();
	}


}

?>
