<?php

require_once(dirname(dirname(dirname(__FILE__))).'/config.php');
require_once(dirname(__FILE__).'/lib.php');

$id = optional_param('id', 0, PARAM_INT); // Course_module ID, or
$n  = optional_param('n', 0, PARAM_INT);  // ... pluginmaster instance ID - it should be named as the first character of the module.

if ($id) {
    $cm         = get_coursemodule_from_id('pluginmaster', $id, 0, false, MUST_EXIST);
    $course     = $DB->get_record('course', array('id' => $cm->course), '*', MUST_EXIST);
    $pluginmaster  = $DB->get_record('pluginmaster', array('id' => $cm->instance), '*', MUST_EXIST);
} else if ($n) {
    $pluginmaster  = $DB->get_record('pluginmaster', array('id' => $n), '*', MUST_EXIST);
    $course     = $DB->get_record('course', array('id' => $pluginmaster->course), '*', MUST_EXIST);
    $cm         = get_coursemodule_from_instance('pluginmaster', $pluginmaster->id, $course->id, false, MUST_EXIST);
} else {
    error('You must specify a course_module ID or an instance ID');
}

require_login($course, true, $cm);

$event = \mod_pluginmaster\event\course_module_viewed::create(array(
    'objectid' => $PAGE->cm->instance,
    'context' => $PAGE->context,
));
$event->add_record_snapshot('course', $PAGE->course);
$event->add_record_snapshot($PAGE->cm->modname, $pluginmaster);
$event->trigger();

$idConference = optional_param('conference', 0, PARAM_INT);
include('bigbluebutton-v0.php');

//$bbb=new BigBlueButton();
//var_dump($bbb);
$conference   = $DB->get_record('conference', array('id' => $idConference), '*', MUST_EXIST);
$res=createOnServer($conference);
if($res==true){
	
	$url=getURLParticipant($conference,$USER->email);
	redirection($url);
}
else echo "Echec";

function redirection($filename) {
    if (!headers_sent())
        header('Location: '.$filename);
    else {
        echo '<script type="text/javascript">';
        echo 'window.location.href="'.$filename.'";';
        echo '</script>';
        echo '<noscript>';
        echo '<meta http-equiv="refresh" content="0;url='.$filename.'" />';
        echo '</noscript>';
    }
}


function createOnServer($conference){
$bbb = new BigBlueButton();
$rec='false';
//if($conference->getRecord()==1)
	//$rec='true';
//$naw= (new \DateTime())->getTimestamp();
//$stop_date=($conference->getDateDebut()->getTimestamp())+$conference->getDuration()*60;
//$duree=$stop_date-$naw;
$duree=3600;
$creationParams = array(
	'meetingId' => $conference->meetingid."", 					
	'meetingName' => $conference->name, 	
	'attendeePw' => $conference->attendeepw, 					
	'moderatorPw' => $conference->moderatorpw, 					
	'welcomeMsg' => $conference->welcome, 					
	'dialNumber' => '', 					
	'voiceBridge' => '', 					
	'webVoice' => '', 						
	'logoutUrl' => '', 						
	'maxParticipants' => $conference->maxparticipants, 				
	'record' => true, 					
	'duration' => $duree, 
	'createTime' => '', 
						
	//'meta_category' => '', 				// Use to pass additional info to BBB server. See API docs.
);

// Create the meeting and get back a response:
$itsAllGood = true;
try {$result = $bbb->createMeetingWithXmlResponseArray($creationParams);}
	catch (Exception $e) {
		//echo 'Caught exception: ', $e->getMessage(), "\n";
		
		$itsAllGood = false;
	}

if ($itsAllGood == true) {
	// If it's all good, then we've interfaced with our BBB php api OK:
	if ($result == null) {
	var_dump($creationParams);
die();
		// If we get a null response, then we're not getting any XML back from BBB.
		$itsAllGood == false;
	}	
	else { 
	// We got an XML response, so let's see what it says:
	//print_r($result);
			if ($result['returncode'] == 'SUCCESS') {
				
			
				$itsAllGood == true;
			}
			else {

				$itsAllGood == false;
			}
	}
}

return $itsAllGood;
}


function getURLParticipant($conference,$emailP){
	$bbb = new BigBlueButton();
	
	$joinParams = array(
	'meetingId' => $conference->meetingid, 			// REQUIRED - We have to know which meeting to join.
	'username' => $emailP,	       // REQUIRED - The user display name that will show in the BBB meeting.
	'password' => $conference->attendeepw,				// REQUIRED - Must match either attendee or moderator pass for meeting.
	'createTime' => '',				// OPTIONAL - string
	'userId' => '',					// OPTIONAL - string
	'webVoiceConf' => ''			// OPTIONAL - string
);
// Get the URL to join meeting:
$itsAllGood = true;
try {$result = $bbb->getJoinMeetingURL($joinParams);}
	catch (Exception $e) {
		
		$itsAllGood = false;
	}


if($itsAllGood==true)
	return $result;
else
    return "";	
	
}

function getURLModerateur($conference,$emailM){
	$bbb = new BigBlueButton();
	
	$joinParams = array(
	'meetingId' => $conference->meetingid, 			// REQUIRED - We have to know which meeting to join.
	'username' => $emailM,	       // REQUIRED - The user display name that will show in the BBB meeting.
	'password' => $conference->moderatorpw,				// REQUIRED - Must match either attendee or moderator pass for meeting.
	'createTime' => '',				// OPTIONAL - string
	'userId' => '',					// OPTIONAL - string
	'webVoiceConf' => ''			// OPTIONAL - string
);
// Get the URL to join meeting:
$itsAllGood = true;
try {$result = $bbb->getJoinMeetingURL($joinParams);}
	catch (Exception $e) {
		
		$itsAllGood = false;
	}


if($itsAllGood==true)
	return $result;
else
    return "";	
	
}
?>
