<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Prints a particular instance of mastersms
 *
 * You can have a rather longer description of the file as well,
 * if you like, and it can span multiple lines.
 *
 * @package    mod_mastersms
 * @copyright  2016 Your Name <your@email.address>
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

// Replace mastersms with the name of your module and remove this line.

require_once(dirname(dirname(dirname(__FILE__))).'/config.php');
require_once(dirname(__FILE__).'/lib.php');

$id = optional_param('id', 0, PARAM_INT); // Course_module ID, or
$n  = optional_param('n', 0, PARAM_INT);  // ... mastersms instance ID - it should be named as the first character of the module.

if ($id) {
    $cm         = get_coursemodule_from_id('mastersms', $id, 0, false, MUST_EXIST);
    $course     = $DB->get_record('course', array('id' => $cm->course), '*', MUST_EXIST);
    $mastersms  = $DB->get_record('mastersms', array('id' => $cm->instance), '*', MUST_EXIST);
} else if ($n) {
    $mastersms  = $DB->get_record('mastersms', array('id' => $n), '*', MUST_EXIST);
    $course     = $DB->get_record('course', array('id' => $mastersms->course), '*', MUST_EXIST);
    $cm         = get_coursemodule_from_instance('mastersms', $mastersms->id, $course->id, false, MUST_EXIST);
} else {
    error('You must specify a course_module ID or an instance ID');
}

require_login($course, true, $cm);

$event = \mod_mastersms\event\course_module_viewed::create(array(
    'objectid' => $PAGE->cm->instance,
    'context' => $PAGE->context,
));
$event->add_record_snapshot('course', $PAGE->course);
$event->add_record_snapshot($PAGE->cm->modname, $mastersms);
$event->trigger();

// Print the page header.

$PAGE->set_url('/mod/mastersms/addsms_selection.php', array('id' => $cm->id));
$PAGE->set_title(format_string($mastersms->name));
$PAGE->set_heading(format_string($course->fullname));

/*
 * Other things you may want to set - remove if not needed.
 * $PAGE->set_cacheable(false);
 * $PAGE->set_focuscontrol('some-html-id');
 * $PAGE->add_body_class('mastersms-'.$somevar);
 */

// Output starts here.
echo $OUTPUT->header();

// Conditions to show the intro can change to look for own settings or whatever.
if ($mastersms->intro) {
    echo $OUTPUT->box(format_module_intro('mastersms', $mastersms, $cm->id), 'generalbox mod_introbox', 'mastersmsintro');
}

echo '<form action="envoyer.php?id='.$id.'"  method="post" >';
echo '<table class="table">';
echo '<h1>Envoyer un nouveau Message</h1>';

$users = get_enrolled_users(context_course::instance($course->id), '');
echo '<tr>';
echo '<th>Selectionner</th>';
echo '<th>Nom</th>';
echo '<th>Prenom</th>';
echo '<th>Email</th>';
echo '<th>Phone SIP</th>';
echo '</tr>';

if($users!=null){
	foreach( $users as $us){
		echo '<tr>';
                //echo '<td class="corps"><input type="checkbox" name="phone" value="'.$us->phone2.'"/></td>';
                echo '<td class="corps"><input type="checkbox" name="phone[]" value="'.$us->phone2.'"/></td>';
		echo '<td>'.$us->firstname.'</td>';
                echo '<td>'.$us->lastname.'</td>';
                echo '<td>'.$us->email.'</td>';
                echo '<td>'.$us->phone2.'</td>';
                echo '</tr>';	
	}
	echo '<tr>';
	echo '<td><textarea rows="8" cols="40" name="message" id="message"></textarea></td>';
	echo '</tr></br>';
	echo '<tr>';
	echo '<td><button type="submit" rows="8" cols="40">Envoyer message</button></td>';
	echo '</tr>';
	echo '</table></form>';
}        


function redirection($filename) {
    if (!headers_sent())
        header('Location: '.$filename);
    else {
        echo '<script type="text/javascript">';
        echo 'window.location.href="'.$filename.'";';
        echo '</script>';
        echo '<noscript>';
        echo '<meta http-equiv="refresh" content="0;url='.$filename.'" />';
        echo '</noscript>';
    }
}


// Finish the page.
echo $OUTPUT->footer();
