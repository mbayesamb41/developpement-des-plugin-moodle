<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Prints a particular instance of mastersms
 *
 * You can have a rather longer description of the file as well,
 * if you like, and it can span multiple lines.
 *
 * @package    mod_mastersms
 * @copyright  2016 Your Name <your@email.address>
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

// Replace mastersms with the name of your module and remove this line.

require_once(dirname(dirname(dirname(__FILE__))).'/config.php');
require_once(dirname(__FILE__).'/lib.php');

$id = optional_param('id', 0, PARAM_INT); // Course_module ID, or
$n  = optional_param('n', 0, PARAM_INT);  // ... mastersms instance ID - it should be named as the first character of the module.

if ($id) {
    $cm         = get_coursemodule_from_id('mastersms', $id, 0, false, MUST_EXIST);
    $course     = $DB->get_record('course', array('id' => $cm->course), '*', MUST_EXIST);
    $mastersms  = $DB->get_record('mastersms', array('id' => $cm->instance), '*', MUST_EXIST);
} else if ($n) {
    $mastersms  = $DB->get_record('mastersms', array('id' => $n), '*', MUST_EXIST);
    $course     = $DB->get_record('course', array('id' => $mastersms->course), '*', MUST_EXIST);
    $cm         = get_coursemodule_from_instance('mastersms', $mastersms->id, $course->id, false, MUST_EXIST);
} else {
    error('You must specify a course_module ID or an instance ID');
}

require_login($course, true, $cm);

$event = \mod_mastersms\event\course_module_viewed::create(array(
    'objectid' => $PAGE->cm->instance,
    'context' => $PAGE->context,
));
$event->add_record_snapshot('course', $PAGE->course);
$event->add_record_snapshot($PAGE->cm->modname, $mastersms);
$event->trigger();

// Print the page header.

$PAGE->set_url('/mod/mastersms/addsms_tout.php', array('id' => $cm->id));
$PAGE->set_title(format_string($mastersms->name));
$PAGE->set_heading(format_string($course->fullname));

/*
 * Other things you may want to set - remove if not needed.
 * $PAGE->set_cacheable(false);
 * $PAGE->set_focuscontrol('some-html-id');
 * $PAGE->add_body_class('mastersms-'.$somevar);
 */

// Output starts here.
echo $OUTPUT->header();

// Conditions to show the intro can change to look for own settings or whatever.
if ($mastersms->intro) {
    echo $OUTPUT->box(format_module_intro('mastersms', $mastersms, $cm->id), 'generalbox mod_introbox', 'mastersmsintro');
}


include('form/add_form.php');
include('sms.php');
//creation d‘un objet simplehtml_form 
$mform = new add_form("addsms_tout.php?id=".$_GET['id']);
$apikey = "f8dfa6472708a1a0f766";
$sms = new sendsmsdotpk($apikey);
//Le traitement et l'affichage des formulaires sont effectués ici
if($mform->is_cancelled()) {
    //Effectuez l'opération d'annulation de formulaire, si l‘utilisateur à cliqué sur le        //bouton d'annulation du formulaire
} else if ($fromform = $mform->get_data()) {

        $p=new stdClass();
        $p->message=$fromform->message;
        $p->dateenvoi=$fromform->dateDebut;
        $p->instance=$cm->instance;
        $users = get_enrolled_users(context_course::instance($course->id), '');
        if($users!=null){
            foreach( $users as $us){
                $p->user = $us->phone2;
                $sms->sendsms($us->phone2, $p->message, 0);
                $idRec=$DB->insert_record('sms', $p, false);
            }
        }

        redirection("view.php?id=".$_GET['id']."&message=Votre message est envoyé");

} else {
  // Cette branche est exécutée si le formulaire est soumis, mais les données ne sont pas validées et le formulaire doit être reproduit automatiquement   
// ou sur le premier affichage du formulaire.
 //Définir les données par défaut (le cas échéant)
  //$mform->set_data($toform);
  //afficher le formulaire
  $mform->display();
}

function redirection($filename) {
    if (!headers_sent())
        header('Location: '.$filename);
    else {
        echo '<script type="text/javascript">';
        echo 'window.location.href="'.$filename.'";';
        echo '</script>';
        echo '<noscript>';
        echo '<meta http-equiv="refresh" content="0;url='.$filename.'" />';
        echo '</noscript>';
    }
}




// Finish the page.
echo $OUTPUT->footer();
