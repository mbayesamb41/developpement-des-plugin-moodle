<?php

require_once("$CFG->libdir/formslib.php");

class add_form extends moodleform {

        public function definition(){

        global $CFG;
                $mform = $this->_form;
                $attributes=array('rows'=>'10','cols'=>'90');
                $mform->addElement('header', 'general', get_string("newmessage","mastersms"));
                $mform->addElement('date_time_selector', 'dateDebut', get_string('begindate', 'mastersms'));
                $mform->addElement('textarea', 'message', get_string('message', 'mastersms'), $attributes);
                $mform->setType('message', PARAM_TEXT);

                $this->add_action_buttons($cancel = true, $submitlabel='Envoyer');
        }
}
?>
