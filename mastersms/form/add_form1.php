<?php

require_once("$CFG->libdir/formslib.php");

class add_form extends moodleform {

        public function definition(){

           global $CFG;
                $mform = $this->_form;
		$attributes=array('rows'=>'8','cols'=>'90');
		$mform->addElement('header', 'general', get_string("newmessage","mastersms"));
                $mform->addElement('date_time_selector', 'dateDebut', get_string('begindate', 'mastersms'));


		/*
                $options[79010]= "79010";
                $options[79011]= "79011";
                $options[79012]= "79012";
                $options[79013]= "79013";
                $options[79014]= "79014";
                $options[79015]= "79015"; */
		
		$options = array(
    			'79010' => '79010',
    			'79011' => '79011',
    			'79012' => '79012',
    			'79013' => '79013',
    			'79014' => '79014',
    			'79015' => '79015'
		);
		$select = $mform->addElement('select', 'destinations', get_string('destinations', 'mastersms'), $options);
		// This will select the colour blue.
		$select->setSelected('79010');

                //$mform->addElement('select', 'destinations', get_string('destinations', 'mastersms'), $options);

		//$mform->addElement('text', 'destinations',get_string("destinations","mastersms"), 'maxlength="100" size="20" ');
                $mform->setType('destinations', PARAM_TEXT);
		$mform->addElement('textarea', 'message', get_string('message', 'mastersms'), $attributes);
                $mform->setType('message', PARAM_TEXT);

		$this->add_action_buttons($cancel = true, $submitlabel='Envoyer');
        }

}

?>
